'use strict';

// >>> MISE NE PLACE DU SERVEUR WEB <<<
const express = require('express');
const app = express();
app.use(express.static(__dirname + '/public'));

const https = require('https');
const fs = require('fs');
const httpsOptions = {
	key: fs.readFileSync('./key.pem'),
	cert: fs.readFileSync('./cert.pem'),
	passphrase : "yesUcan"
}
const myWebServer = https.createServer(httpsOptions, app);

app.get('/', function(req, res, next) {
	res.sendFile(__dirname + '/public/index.html')
});

const port = process.env.PORT || 3000;

myWebServer.listen(port);
console.log(`Serveur en écoute sur https://localhost:${port}`);

// >>> MISE EN PLACE DU PORT SERIE <<<
const { SerialPort } = require('serialport')
const { ReadlineParser } = require('@serialport/parser-readline')

// Ouvrir le port série vers l'Arduino
// (path : 'COM?' sur Windows et path : '/dev/serial0' sur Raspberry Pi OS
let mySerialPort = new SerialPort({
//	path:'COM3'
	path:'/dev/ttySocat0'
	, dataBits:8
	, baudRate:9600
	, stopBits:1
	, parity:'none'
});

// Mettre en place l'interpréteur de données reçues (lignes terminées par '\r')
const parser = mySerialPort.pipe(new ReadlineParser({
	delimiter: '\r'
	, encoding: 'ascii'
}));

// Appliquer l'interpréteur au flux des données reçues sur le port série
mySerialPort.pipe(parser);

// Définir et Mettre en place les callbacks du port série
function showSerialPortOpen(err) {
	if (err) {
		return console.log('Error ouverture port série : ', err.message);
	}
    console.log('Port série ouvert. Débit: ' + mySerialPort.baudRate);
}

function readSerialData(data) {
    console.log('Trame liaison série reçue : [' + data + ']');

	// Expression régulière pour chercher $D0 ou $D1 dans la trame reçue
	const regexBP = /\$D[0-1]/;

	// SI $D0 ou $D1 trouvé dans la trame reçue ALORS
	if (data.search(regexBP) !== -1) {
		// Emettre message websocket 'bpFeedback' avec valeur 0 ou 1 vers l'IHM
		if (data.indexOf('0') !== -1) {
			myWebsocketServer.emit('bpFeedback', 0);
		} else {
			myWebsocketServer.emit('bpFeedback', 1);
		}
	}

	// Expression régulière pour chercher $S### dans la trame reçue 
	// ('#' représentant un chiffre)
	const regexCurseur = /\$S[0-9]{3}/;

	// SI $S### trouvé dans trame reçue ALORS
	if (data.search(regexCurseur) !== -1) {
		// Emmettre message websocket 'curseurFeedback' avec valeur du curseur
		// vers l'IHM
		let val = parseInt(data.substring(3, 6));
		myWebsocketServer.emit('curseurFeedback', val);
	}
}

function showSerialPortClose() {
    console.log('port closed.');
}

function showSerialPortError(error) {
    console.log('Erreur port série : ' + error);
}

mySerialPort.on('open', (err) => {showSerialPortOpen(err)});
mySerialPort.on('close', showSerialPortClose);
mySerialPort.on('error', showSerialPortError);
parser.on('data', readSerialData);

// >>> MISE EN PLACE DU SERVEUR WEBSOCKET <<<
const myWebsocketServer = require('socket.io')(myWebServer);

myWebsocketServer.on('connection', function(myWebsocketClient) {

	// Gestionnaire pour évènement 'join'
	// (-> acquittement de connexion par le client)
	myWebsocketClient.on('join', function(data) {
		console.log(">> " + data + " <<" );
	});

	// Gestionnaire pour évènement 'bp' 
	// (-> bouton poussoir enfoncé/relâché sur l'IHM)
	myWebsocketClient.on('bp', function(data) {
		// Afficher détails de l'évènement
		switch(data) {
			case 0 :
				console.log("Requête BP : Extinction DEL");
				break;

			case 1 :
				console.log("Requête BP : Allumage DEL");
				break;

			default:
				console.log("Requête BP nom valide");
				break;
		}
		// Construite la trame à envoyer sur le port série
		let frame = '\$D' + data + '\r\n';
		// Envoyer la trame sur le port série
		mySerialPort.write(frame);

	});	

	// Gestionnaire pour évènement 'curseur' 
	// (-> curseur modifié sur l'IHM)
	myWebsocketClient.on('curseur', function(data) {
		// formater la valeur du curseur reçue dans le message sur 3 chiffres
		let paddedValue = data.padStart(3, '0');
		// Construire la trame à envoyer sur le port série
		let frame = "\$S" + paddedValue + '\r\n';
		// Envoyer la trame
		mySerialPort.write(frame);
	});
});



