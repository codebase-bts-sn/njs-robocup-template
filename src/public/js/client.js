{
    // Fonction d'anti-rebond
    // Permet de ne pas prendre en compte tous les évènements liés au déplacement d'un slider par ex.
    // Voir : https://www.educative.io/edpresso/how-to-use-the-debounce-function-in-javascript
    const debounce = (func, wait, immediate) => {
        var timeout;

        return function executedFunction() {
            var context = this;
            var args = arguments;

            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };

            var callNow = immediate && !timeout;

            clearTimeout(timeout);

            timeout = setTimeout(later, wait);

            if (callNow) func.apply(context, args);
        };
    };

    var mySlider = document.querySelector('#curseur');
    var mySliderValue = document.querySelector("#valeurCurseur");
    var myButton = document.querySelector('#bouton');
    var myDel = document.querySelector('#del');

    mySlider.value = 0;
    mySliderValue.innerHTML = 0;

    var socket = window.io.connect(window.location.hostname + ':' + 3000);
    
    mySlider.addEventListener('input'
        , debounce(function () {
            console.log(`value : ${mySlider.value}`);
            socket.emit('curseur', mySlider.value);
        }
            , 50
        )
        , false
    );

    myButton.addEventListener('mousedown'
        , () => {
            socket.emit('bp', 1);
        }
        , false
    );

    myButton.addEventListener('mouseup'
        , () => {
            socket.emit('bp', 0)
        }
        , false
    );

    socket.on('connect', function () {
        console.log("connect� au serveur");
        socket.emit('join', 'Client connecté !');
    });

    socket.on('bpFeedback', function (data) {
        console.log("bpFeedback : " + data);
        switch (data) {
            case 0:
                myDel.classList.remove('on');
                myDel.className = 'off';
                break;

            case 1:
                myDel.classList.remove('off');
                myDel.className = 'on';
                break;

            default:
                console.log("erreur feedback bouton poussoir");
                break;
        }
    });

    socket.on('curseurFeedback', function (data) {
        console.log("curseurFeedback : " + data);
        mySliderValue.innerHTML = data;
    });


}