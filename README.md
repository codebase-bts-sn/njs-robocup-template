# NJS-ROBOCUP-TEMPLATE

Base de développement pour l'application NodeJS dans le cadre du projet "Robocup".

![IHM](img/screenshot.png "IHM")

L'application propose une IHM web sur laquelle figure un bouton poussoir et un curseur.

Lorsque le bouton poussoir est enfoncé, un message est envoyé à l'application NodeJS via websocket. L'application construit alors une trame ASCII ('$B1\r') puis l'envoie sur la liaison série sur laquelle est branchée la carte Arduino qui pilote les moteurs. Celle-ci doit alors renvoyer la trame à l'identique pour indiquer sa bonne prise en compte. L'application NodeJS renvoie alors un message via Websocket sur la page web qui réagit en allumant en vert la DEL située sous le bouton poussoir.

Lorsque le bouton poussoir est relâché, la même séquence se déroule avec un message websocket et une trame série différente. Sur réception d'une réponse correcte, la page web se met à jour en éteignant la DEL.

Le même principe est utilisé pour la modification du curseur sur l'IHM web. Il est à noter qu'une fonction JS permet de filtrer les déplacements du curseur de façon à ne prendre en compte sa valeur que si elle demeure stable assez longtemps (-> évite d'envoyer trop de trames sur la liaison série)

Pour exécuter l'application :

. Installer les paquets nécessaire pour l'application : `npm intall`

. Modifier le fichier `app.js` pour définir le port série à utiliser.

. Exécuter l'application : `node app.js`

Ci-dessous figure le diagrame de séquence UML de l'application :

![Diagramme de séquence](img/diag-sequence.svg  "Diagramme de séquence")

